#include <stdint.h>

static void putuint(uint32_t i, int mem_pos) {
	*((volatile uint32_t *)mem_pos) = i;
}

void linked_list(){
	uint32_t num = 0;
	uint32_t pointer = 0x4000;
	uint32_t data_pos = 0x4004;
	uint32_t jump = 0x0008;
	uint32_t aux = 0;
	uint32_t impar [5] = {0,0,0,0,0};
	uint32_t read = 0x4000;
	uint32_t reg = 0;
	uint32_t i = 0;
	while(aux <= 0xFFFC){
		aux = pointer+jump;
		putuint(aux,pointer);
		putuint(num,data_pos);
		pointer = aux;
		data_pos = data_pos + jump;
		num ++;
	}
	while(read <= 0xFFF8){
		 reg = *((volatile uint32_t *)(read + 4));
        if (reg%2 == 1){
            impar[i] = reg;
            i++;
            if (i == 5){
                i = 0;
            }
        }
        read = *((volatile uint32_t *)read);
	}
}

void main() {
	uint32_t counter = 0;
	const uint32_t limit = 10;
	while (1) {
		counter = 0;
		linked_list();
		while (counter < limit) {
			counter++;
		}
	}
}
