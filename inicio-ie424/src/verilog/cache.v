module cache #(parameter CACHE_SIZE=2048, parameter BLOCK_SIZE=16)(//Ambos Bytes(
    input 	      clk,
    input 	      resetn,
    //Registers
    input cpu_valid,
    input mem_intr,
    output reg cpu_ready,
    input [31:0] cpu_addr,
    input [31:0] cpu_wdata,
    input [3:0] cpu_wstrb,
    output reg [31:0] cpu_rdata,
    //Main Memory
    output reg mem_valid,
    input mem_ready,
    output reg [31:0] mem_addr,
    output reg [31:0] mem_wdata,
    output reg [3:0]  mem_wstrb,
    input [31:0] mem_rdata,
    output reg [20:0] hits,
    output reg [20:0] miss
);

    //Parametros de cache
    parameter WAYS = 2;
    parameter WORD_SIZE = 4;
    parameter WORD_BIT_SIZE = 8*WORD_SIZE;          //32
    parameter NUM_BLOCK  = CACHE_SIZE/BLOCK_SIZE;   //128
    parameter OFFSET_SIZE = $clog2(BLOCK_SIZE);     //3
    parameter WORDS_BLOCK = BLOCK_SIZE/WORD_SIZE;   //2
    parameter SETS = NUM_BLOCK/WAYS;                //64
    parameter INDEX_SIZE = $clog2(SETS);            //6
    parameter TAG_SIZE = 32-INDEX_SIZE-OFFSET_SIZE; //23
    parameter CACHE_TAG_SIZE = 1+1+1+TAG_SIZE;      //26      db + valid + LRU + tag

    //States: codif One hot 
    parameter IDLE = 1;
    parameter READ = 2;
    parameter WRITE = 4;
    parameter READ_MISS = 8;
    parameter WRITE_MISS = 16;
    parameter MEM_ACCESS = 32; 
    parameter MEM_WRITE = 64;  
    parameter WRITE_BACK = 128;
    parameter LRU = 256;

    reg [CACHE_TAG_SIZE-1:0] reg_address [SETS-1:0][WAYS-1:0]; 
    reg [31:0] cache_data [SETS-1:0][WAYS-1:0];
    reg [TAG_SIZE-1:0] tag; 
    reg [INDEX_SIZE-1:0] index;
    reg [OFFSET_SIZE-1:0] offset;
    reg [8:0] STATE;
    reg [31:0] temporal_address;
    reg [OFFSET_SIZE-1:0] offset_temp;
    reg [OFFSET_SIZE:0] blockCounter;
    reg [31:0] temporal_address_W;
    reg offset_temp_w;
    reg [OFFSET_SIZE:0] counter_W;
    reg [TAG_SIZE-1:0] tg_idx [WAYS-1:0];
    reg [WAYS-1:0] way_valid;
    reg [WAYS-1:0] way_LRU;
    reg [WAYS-1:0] reg_pos;
                 
    integer i,j;
    always @(posedge clk) begin
        if (resetn == 0) begin
            miss <= 0;
            hits <= 0;
            STATE <= IDLE;
            for (i = 0; i < SETS; i = i + 1) begin
                for (j = 0; j < WAYS; j = j + 1) begin
                    reg_address[i][j] <= 0;
                end
            end
        end
        else begin
            case (STATE)
                IDLE: begin
                    tag <= cpu_addr[31:31-TAG_SIZE];
                    index <= cpu_addr[INDEX_SIZE+OFFSET_SIZE-1:OFFSET_SIZE];
                    offset <= cpu_addr[OFFSET_SIZE-1:0];
                    if (cpu_valid) begin
                        for (i = 0; i < WAYS; i = i + 1) begin
                            if (reg_address[index][i][TAG_SIZE-1:0] == tag && reg_address[index][i][CACHE_TAG_SIZE-2]) begin
                                tg_idx[i] <= reg_address[index][i][TAG_SIZE-1:0];
                                way_valid[i] <= reg_address[index][i][CACHE_TAG_SIZE-2]; 
                                way_LRU[i] <= reg_address[index][i][CACHE_TAG_SIZE-3];
                                reg_pos <= i;
                            end
                            else begin
                                tg_idx[i] <= reg_address[index][i][TAG_SIZE-1:0];
                                way_valid[i] <= reg_address[index][i][CACHE_TAG_SIZE-2];
                                way_LRU[i] <= reg_address[index][i][CACHE_TAG_SIZE-3];
                                reg_pos <= i;
                            end
                        end     
                        if (cpu_wstrb)  begin
                            STATE <= WRITE;
                        end
                        if (!cpu_wstrb) begin
                            STATE <= READ;
                        end
                    end        
                end              

                READ: begin                     
                    if (tg_idx[reg_pos] == tag && way_valid[reg_pos]) begin
                        cpu_ready <= 1;
	                    cpu_rdata <= cache_data[reg_pos][offset>>2][index];
                        hits <= hits+1;
                        STATE <= LRU;
                    end
                    else begin              //Miss
                        miss <= miss + 1;
                        temporal_address <= cpu_addr & (32'hFFFFFFFF<<(3));
                        offset_temp <= 0;
                        blockCounter <= 0;
                        offset_temp_w <= 0;
                        counter_W <= 0;
                        if(!reg_address[index][reg_pos][CACHE_TAG_SIZE-2])begin
                            reg_address[index][reg_pos][CACHE_TAG_SIZE-2] <= 1;
                            reg_address[index][reg_pos][TAG_SIZE-1:0] <= tag; 
                            STATE <= READ_MISS;                       
                        end
                        else begin
                            if (reg_address[index][reg_pos][CACHE_TAG_SIZE-3]) begin
                                temporal_address_W <= ((reg_address[index][reg_pos][TAG_SIZE-1]<<(INDEX_SIZE))+index)<<3;
                                STATE <= WRITE_BACK;
                            end else begin
                                STATE <= READ_MISS;
                            end
                        end
                        reg_address[index][reg_pos][CACHE_TAG_SIZE-1] <= 1'b1;
                        reg_address[index][reg_pos][TAG_SIZE-1:0] <= tag;
                    end
                end

                WRITE: begin
                    if (tg_idx[reg_pos] == tag && way_valid[reg_pos]) begin
                        hits <= hits + 1;
                        cache_data[0][offset][index] <= cpu_wdata;              
                        cpu_ready <= 1;
                        STATE <= LRU;
                    end
                    else begin
                        miss <= miss + 1;
                        temporal_address <= cpu_addr & (32'hFFFFFFFF<<(3));
                        offset_temp <= 0;
                        blockCounter <= 0;
                        offset_temp_w <= 0;
                        counter_W <= 0;
                        if (!reg_address[index][reg_pos][CACHE_TAG_SIZE-2]) begin
                            reg_address[index][reg_pos][CACHE_TAG_SIZE-2] <= 1'b1;
                            reg_address[index][reg_pos][TAG_SIZE-1:0] <= tag;
                            STATE <= LRU;
                        end else begin
                            if (!reg_address[index][reg_pos][CACHE_TAG_SIZE-3]) begin
                                temporal_address_W <= ((reg_address[index][reg_pos][TAG_SIZE-1:0]<<(INDEX_SIZE))+index)<<3;
                                STATE <= WRITE_MISS;
                            end else begin
                                temporal_address_W <= ((reg_address[index][reg_pos][TAG_SIZE-1:0]<<(INDEX_SIZE))+index)<<3;
                                STATE <= WRITE_BACK;
                            end
                        end
                    end
                    reg_address[index][reg_pos][CACHE_TAG_SIZE-1] <= 1'b1;
                    reg_address[index][reg_pos][TAG_SIZE-1:0] <= tag;
                end
                
                READ_MISS: begin
                    cpu_ready <= 1;
                    cpu_rdata <= cache_data[reg_pos][offset>>2][index];
                    STATE <= MEM_ACCESS;    
                end
                
                WRITE_MISS: begin
                    cache_data[reg_pos][offset>>2][index] <= temporal_address_W;
                    cpu_ready <= 1;
                    STATE <= LRU;
                end

                MEM_ACCESS: begin
                    mem_valid <= 1;
                    mem_addr <= temporal_address;
                    mem_wdata <= cpu_wdata;
                    mem_wstrb <= 0;
                    if (!mem_ready) begin
                        cache_data[reg_pos][offset_temp_w][index] <= mem_rdata;
                        cpu_rdata <= mem_rdata;
                    end else begin
                        STATE <= LRU;
                    end
                end

                MEM_WRITE: begin
                    mem_addr <= temporal_address_W;
                    mem_wdata <= cache_data[reg_pos][offset_temp_w][index];
                    if (mem_wstrb) begin
                        mem_wdata <= cpu_wdata;
                        STATE <= LRU;
                        cpu_ready <= 1;
                    end                
                end
                
                WRITE_BACK: begin
                    if (mem_ready) begin    
                        mem_wstrb <= 4'hF;
                        mem_valid <= 1;
                        STATE <= MEM_WRITE;
                    end
                    else begin
                        STATE <= WRITE_MISS;
                    end
                end

                LRU: begin
                    reg_address[index][reg_pos][CACHE_TAG_SIZE-3] <= 0;		 
                    reg_address[index][reg_pos + 1][CACHE_TAG_SIZE-3] <= 1;	 
                    reg_address[index][reg_pos][CACHE_TAG_SIZE-1] <= 0;
                    STATE <= IDLE;
                end
                
                default: begin
                    STATE <= IDLE;
                end

            endcase     
        end
    end

    
endmodule
